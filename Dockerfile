FROM openjdk:17.0.1-jdk-slim
WORKDIR /gradle
COPY --chown=gradle:gradle . /gradle
RUN ./gradlew clean build
ARG JAR_FILE=build/libs/*.jar
WORKDIR /opt/app
RUN cp /gradle/${JAR_FILE} schedule-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","schedule-0.0.1-SNAPSHOT.jar"]
